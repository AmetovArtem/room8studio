﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Factory;
using Control;
using UnityEngine.UI;
using System;

namespace Game
{
    public class GameManager : MonoBehaviour
    {
        [SerializeField]
        private InputController InputController;

        [SerializeField] private float Timer = 10;

        [SerializeField] private Text Timetext;

        [SerializeField]
        public Text ponyText;

        [SerializeField]
        private int PonyCount = 10;

        [SerializeField]
        private int BonusCount = 3;

        private int _score = 0;

        private BonusController _bonusController;
        
        public List<PonyController> _ponyList = new List<PonyController>();

        private List<PlayerController> _dogList = new List<PlayerController>();

        private List<BonusController> _bonusList = new List<BonusController>();

        private PlayerController _player;

        private void Start()
        {
            InputController.OnClick += OnClick;
            
            for (int i = 0; i < 2; i++)
            {
                var player = GameFactory.GetDog();
                _dogList.Add(player);

            }
            
            StartCoroutine(InstantiatePony(3));
        }
        
        public IEnumerator InstantiatePony(float time)
        {
            for (int i = 0; i < PonyCount; i++)
            {
                var pony = GameFactory.GetPony();
                pony.OnFence += OnPonyFence;
                _ponyList.Add(pony);

                yield return new WaitForSeconds(time);
                
            }
            
        }

        private void Update()
        {
            
            
        }

        private void OnPonyFence(PonyController obj)
        {
            obj.OnFence -= OnPonyFence;
            _score++;
            ponyText.text = _score.ToString();

            if (_score > 5)
            {

                var bonus = GameFactory.GetBonus();

                if (Timetext != null)
                {
                    Timetext.gameObject.SetActive(true);
                }
                Timer -= Time.deltaTime;

                Timetext.text = Mathf.Round(Timer).ToString();

                if (Timer <= 0)
                {

                    Timer = 0f;

                    Destroy(this.gameObject);

                }

            }
        }

        private void OnClick(Vector3 clickPosition, IInteractable interactable)
        {
            PlayerController player = interactable as PlayerController;
            if(player != null)
            {
                if(_player != null)
                {
                    _player.ActivateDog(false);
                }

                _player = player;
                _player.ActivateDog(true);
                return;
            }

            //Debug.Log(clickPosition);
            if (_player != null)
            {
                _player.SetMoveTarget(clickPosition);
            }
        }
    }
}