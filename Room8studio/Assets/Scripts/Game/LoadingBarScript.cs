﻿using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace Game
{
    public class LoadingBarScript : MonoBehaviour
    {
        private bool loadScene = false;
        public string LoadingSceneName;
        public Text loadingText;
        public Slider sliderBar;

        void Start()
        {
            sliderBar.gameObject.SetActive(true);

            loadingText.text = "Loading...";

            StartCoroutine(LoadNewScene(LoadingSceneName));
        }

        IEnumerator LoadNewScene(string sceneName)
        {

            AsyncOperation async = SceneManager.LoadSceneAsync(sceneName);

            while (!async.isDone)
            {
                float progress = Mathf.Clamp01(async.progress / 0.9f);
                sliderBar.value = progress;
                loadingText.text = progress * 100f + "%";
                yield return null;

            }
        }
    }
}