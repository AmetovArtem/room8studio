﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Game
{
    public class BonusController : MonoBehaviour
    {
        private void OnTriggerEnter(Collider other)
        {
            if (other.gameObject.layer == LayerMask.NameToLayer("Dog"))
            {
                Debug.Log("the bonus is collected ");
                Destroy(this.gameObject);
            }
        }
    }
}
