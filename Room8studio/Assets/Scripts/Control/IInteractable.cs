﻿using UnityEngine;

namespace Control
{
    public interface IInteractable
    {
        GameObject CashedGameObject { get; }
    }
}
