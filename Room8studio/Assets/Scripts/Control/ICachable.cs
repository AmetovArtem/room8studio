﻿
namespace Control
{
    public interface ICachable
    {
        bool IsActive { get; }
    }
}
