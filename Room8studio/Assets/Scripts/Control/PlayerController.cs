﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Control
{
    public class PlayerController : MonoBehaviour, IInteractable, ICachable
    {
        [SerializeField] private Collider _collider;

        private Vector2 targetPos;
        float moveSpeed = 3f;

        private bool _isMoving = false;

        public GameObject CashedGameObject
        {
            get
            {
                return gameObject;
            }
        }

        public bool IsActive { get; private set; }

        public void ActivateDog(bool isActive)
        {
            IsActive = isActive;
            //_collider.enabled = !isActive;
        }

        private void Update()
        {
            Move();
        }

        private void Move()
        {
            if(!_isMoving)
            {
                return;

            }
            transform.position = Vector3.Lerp(transform.position, targetPos, moveSpeed * Time.deltaTime);

            var targetDist = Vector3.Distance(transform.position, targetPos);
            
            if (targetDist < 0.01f)
            {
                transform.position = targetPos;
                _isMoving = false;

            }
        }

        public void SetMoveTarget(Vector3 clickPosition)
        {
            targetPos = new Vector3(clickPosition.x, clickPosition.y, 1);

            _isMoving = true;
        }
    }
}
