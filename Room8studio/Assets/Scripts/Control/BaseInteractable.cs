﻿using UnityEngine;

namespace Control
{
    public class BaseInteractable : MonoBehaviour, IInteractable
    {
        public GameObject CashedGameObject
        {
            get
            {
                return gameObject;
            }
        }
    }
}
