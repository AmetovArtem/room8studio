﻿using UnityEngine;
using System;

namespace Control
{
    public class InputController : MonoBehaviour
    {
        [SerializeField] private Camera Camera;
        [SerializeField] private LayerMask LayerMask;

        public event Action<Vector3, IInteractable> OnClick;
        
        private bool _isClick = false;

        private void Update()
        {
            if (Input.GetMouseButton(0))
            {
                Ray ray = Camera.ScreenPointToRay(Input.mousePosition);

                RaycastHit hit;
                
                if (Physics.Raycast(ray, out hit, LayerMask))
                {
                    var interactable = hit.transform.GetComponent<IInteractable>();
                    
                    if (interactable != null)
                    {
                        _isClick = true;
                    }
                }
            }
            

            if (Input.GetMouseButtonUp(0))
            {
                if (_isClick)
                {
                    //Debug.Log("isdrag");

                    _isClick = false;

                    Ray ray = Camera.ScreenPointToRay(Input.mousePosition);

                    RaycastHit hit;
                    
                    if (Physics.Raycast(ray, out hit, LayerMask))
                    {
                        var interactable = hit.transform.GetComponent<IInteractable>();

                        if (interactable != null)
                        {
                            if (OnClick != null)
                            {
                                OnClick(hit.point, interactable);
                            }
                        }
                    }


                }
            }

        }

        private void StartDrag()
        { 
            
        }

        private void Drag()
        {

        }

        private void EndDrag()
        {

        }
    }
}
