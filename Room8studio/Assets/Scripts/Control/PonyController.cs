﻿using Game;
using System;
using UnityEngine;

namespace Control
{
    public class PonyController : MonoBehaviour
    {
        public event Action<PonyController> OnFence;
        
        private Transform _target;
        
        private float _speed = 1f;

        private bool _isInFence;
        
        private void Update()
        {
            if (_target == null)
                return;

            transform.position = Vector2.Lerp(transform.position, _target.position, _speed * Time.deltaTime);

            var dis = Vector2.Distance(transform.position, _target.position);

            if (dis < 0.01f)
            {
                transform.position = transform.position;

            }
        }

        private void OnTriggerEnter(Collider other)
        {
            if (other.gameObject.layer == LayerMask.NameToLayer("Dog"))
            {
                if(_isInFence)
                {
                    return;
                }
                //Debug.Log("!!!");
                var cachable = other.transform.GetComponent<ICachable>();

                if(cachable != null && cachable.IsActive)
                {
                    _target = other.transform;
                }

            }
            else if (other.gameObject.layer == LayerMask.NameToLayer("fence"))
            {
                //Debug.Log("trigered");

                _target = other.transform;

                _isInFence = true;

                if(OnFence != null)
                {
                    OnFence(this);
                }
                
            }
        }
    }
}
