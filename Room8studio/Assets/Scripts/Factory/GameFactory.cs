﻿using Control;
using System.Collections.Generic;
using UnityEngine;
using Game;

namespace Factory
{
    public class GameFactory
    {
        public static PonyController GetPony()
        {
            var prefab = Resources.Load<PonyController>("Prefab/Pony");
            PonyController result = GameObject.Instantiate(prefab);
            
            result.transform.position = GetRandomPonyPosition();

            return result;

        }
        
        public static PlayerController GetDog()
        {
            GameObject result = GameObject.Instantiate(Resources.Load("Prefab/Dog")) as GameObject;

            result.transform.position = GetRandomDogPosition();

            return result.GetComponent<PlayerController>();
        }

        public static BonusController GetBonus()
        {
            var prefab = Resources.Load<BonusController>("Prefab/Bonus");

            BonusController result = GameObject.Instantiate(prefab);
            result.transform.position = GetRandomBonusPosition();
            return result;
        }

        public static Vector3 GetRandomPonyPosition()
        {
            return new Vector3(Random.Range(-4, 10), Random.Range(-6, 6), 0);
        }

        public static Vector3 GetRandomDogPosition()
        {
            return new Vector3(Random.Range(-10, 10), 5, 0);
        }

        public static Vector3 GetRandomBonusPosition()
        {
            return new Vector3(Random.Range(-7, 7), Random.Range(-7, 7), 0);
        }
    }
}
